# Study Path

## Onboarding


### Foundamentals
 - [Git](https://www.w3schools.com/git/)
 - [Docker](https://www.docker.com/101-tutorial/)
 - [Kotlin](https://plugins.jetbrains.com/plugin/17654-atomickotlin)
 - [OAuth2.0](https://auth0.com/intro-to-iam/what-is-oauth-2)
 - [OpenId](https://openid.net/developers/how-connect-works/)
 - [SSH](https://www.digitalocean.com/community/tutorials/how-to-use-ssh-to-connect-to-a-remote-server)
 - BackEnd FrameWorks: [Vertx](https://vertx.io/docs/), [Helidon SE](https://helidon.io/docs/v4/about/doc_overview)
 - FrontEnd FrameWorks: [HTMX](https://htmx.org/), [TailWind](https://tailwindcss.com/), [Vue](https://vuejs.org/)


### 0)  Values
  - Read the [Manifesto for Agile Software Development](http://agilemanifesto.org/)
  - Read the [Principles behind the Agile Manifesto](http://agilemanifesto.org/principles.html)
  - Read the [Manifesto for Software Craftsmanship](http://manifesto.softwarecraftsmanship.org/)
  - Read the [12 Factors](https://12factor.net/)

### 1) Methodologies

#### 1.1) Agile
  - Watch: [7 minutes, 26 seconds and the Fundamental Theorem of Agile Software Development](https://www.youtube.com/watch?v=WSes_PexXcA)
  - Read: [No Silver Bullet](http://worrydream.com/refs/Brooks-NoSilverBullet.pdf)
  - 
#### 1.2) Extreme programming
  - Read **Extreme Programming Explained**:
    - Chapter 1: What is XP?
    - Section 1: Exploring XP
    - [eXtreme Programming values](http://www.extremeprogramming.org/values.html)

#### 1.3) User stories
  - Read [User story templates with step-by-step guidance](https://www.aha.io/roadmapping/guide/requirements-management/what-is-a-good-feature-or-user-story-template)
  - Read [Themes, epics, stories, and tasks](https://www.aha.io/roadmapping/guide/agile/themes-vs-epics-vs-stories-vs-tasks)

#### 1.4) Pair programming
  - Read [On Pair Programming](https://martinfowler.com/articles/on-pair-programming.html)
  - Read [Pair Programming Misconceptions](https://martinfowler.com/bliki/PairProgrammingMisconceptions.html)

#### 1.5) Retrospective
  - Read [Retrospective Fundamentals](https://media.pragprog.com/titles/pkretro/fundamentals.pdf)



### 2) Clean Code
 - Read the definition of  [You Arent Gonna Need It](https://wiki.c2.com/?YouArentGonnaNeedIt)

 - Read **The Pragmatic Programmer**:
   - Chapter 1: A Pragmatic Philosophy
   - Chapter 2: A Pragmatic Approach

 - Read **Clean Code**:
   - Chapter 1: Clean Code
   - Chapter 2: Meaningful Names
   - Chapter 3: Functions
   - Chapter 4: Comments
   - Chapter 6: Objects and Data Structure
   - Chapter 7: Error Handling
   - Chapter 8: Boundaries
   - Chapter 9: Unit Tests
   - Chapter 10: Classes

### 3) TDD (Test driven development)
- Read **Test-Driven Development: by example**:
  - Part II: The xUnit Example
  - Part III: Patterns for Test-Driven Development
 - Watch [The Clean Code Talks - Don't Look For Things!](https://www.youtube.com/watch?v=RlfLCWKxHJ0) (~ 35 minutes)
 - Watch [The Clean Code Talks - "Global State and Singletons](https://www.youtube.com/watch?v=-FRm3VPhseI) (~55 minutes)
 - Read [Mocks Aren't Stubs](https://martinfowler.com/articles/mocksArentStubs.html)
 - Watch [How to Write Clean, Testable Code](https://www.youtube.com/watch?v=XcT4yYu_TTs)
 - Read [Integrated Tests Are a Scam](https://blog.thecodewhisperer.com/permalink/integrated-tests-are-a-scam)

 - [**HANDS ON**] Do (also if you already did) the [String Calculator Kata](https://gitlab.com/cherry-chain/string-calculator-kata)
   - Apply the concepts you learned

 - [**HANDS ON**] Another exercise you can do is [Tennis Kata](https://gitlab.com/cherry-chain/tennis-kata)

### 4) Refactoring
 - Read the first three chapters of  [Improving the design of existing code](https://silab.fon.bg.ac.rs/wp-content/uploads/2016/10/Refactoring-Improving-the-Design-of-Existing-Code-Addison-Wesley-Professional-1999.pdf)
 - [**HANDS ON**] Do (also if you already did) the [Tennis Refactoring Kata](https://github.com/emilybache/Tennis-Refactoring-Kata)

### 5) Continuous integration/Continuous delivery
 - Read [Continuous integration](https://martinfowler.com/articles/continuousIntegration.html)
 - Read [Continuous delivery](https://martinfowler.com/bliki/ContinuousDelivery.html)
 - Read [Software Delivery guide](https://martinfowler.com/delivery.html) (OPTIONAL)
 - Read [GitLab Starter guide on CI/CD](https://docs.gitlab.com/ee/ci/)


### 6) API
  - Read tutorials from [REST API Tutorial](https://restfulapi.net/)
    - [Intro](https://restfulapi.net/)
    - [Architecture](https://restfulapi.net/rest-architectural-constraints/)
    - [Naming Conventions](https://restfulapi.net/resource-naming/)
  - Read [Google's conventions](https://google.aip.dev/general)


### 7) Architecture
 - Read [Hexagonal Architecture](https://alistair.cockburn.us/hexagonal-architecture/)
 - Read [Domain Driven Design](https://matfrs2.github.io/RS2/predavanja/literatura/Avram%20A,%20Marinescu%20F.%20-%20Domain%20Driven%20Design%20Quickly.pdf) (First 5 chapters)
 - Read [Layered Architecture](https://martinfowler.com/bliki/PresentationDomainDataLayering.html)
 - Read [Onion Architecture](https://jeffreypalermo.com/2008/07/the-onion-architecture-part-1/)
 - Read [Site Reliability Engineering](https://sre.google/books/) (OPTIONAL)
 - CQRS
   - Watch [Greg Young - CQRS and Event Sourcing](https://www.youtube.com/watch?v=JHGkaShoyNs)
   - [Exploring CQRS and Event Sourcing](https://www.microsoft.com/en-us/download/details.aspx?id=34774)
   - [CQRS The Example](https://leanpub.com/cqrs)
 - Read Clean Architecture (book)
 - Fundamental of Software Architecture (book)
 - Read [Design Patterns](https://refactoring.guru/design-patterns)
 - More [Design Patterns](https://github.com/iluwatar/java-design-patterns) (OPTIONAL)

### 8) Security
 - Be aware of all the security exploits in this year’s [OWASP Top 10](https://owasp.org/www-project-top-ten/)
 - Be aware of all the most notorious attacks [Mitre Attacks](https://attack.mitre.org/)

### 9) MicroService
 - Read [Microservices: A Definition of This New Architectural Term](https://martinfowler.com/articles/microservices.html)
 - Read first 5 chapters of Microservice Architecture: Aligning Principles, Practices, and Culture

### 10) DevOps
 - Read [What is Infrastructure as code](https://aws.amazon.com/what-is/iac/#:~:text=Infrastructure%20as%20code%20(IaC)%20is%20used%20for%20infrastructure%20automation%20to,to%20set%20up%20infrastructure%20environments.)
 - Read [Introduction to DevOps](https://www.geeksforgeeks.org/introduction-to-devops/)
 - Read [Docker tutorial](https://www.geeksforgeeks.org/introduction-to-docker/)
 - Read [Terraform tutorial](https://www.geeksforgeeks.org/what-is-terraform/)
 - Read [Gitlab tutorial](https://www.geeksforgeeks.org/gitlab/)

### 11) Team and collaboration
 - Read [Smart questions](http://www.catb.org/~esr/faqs/smart-questions.html)
 - Read and watch (OPTIONAL):
   - https://high5test.com/dysfunctions-of-a-team/
   - https://trans4mind.com/download-pdfs/Crucial%20Conversations.pdf
   - https://www.forbes.com/sites/brentgleeson/2019/03/14/15-characteristics-of-high-performance-teams/?sh=2e4cf4f96ae0
   - https://www.youtube.com/watch?v=u6XAPnuFjJc
   - https://www.visualcapitalist.com/50-cognitive-biases-in-the-modern-world/

### 12) Quality Strategy
 - Read [The practical Test Pyramid](https://martinfowler.com/articles/practical-test-pyramid.html)
 - Read [The swiss cheese model](https://blog.korny.info/2020/01/20/the-swiss-cheese-model-and-acceptance-tests.html)
 - Read [Testing strategies in a Microservice Architecture](https://martinfowler.com/articles/microservice-testing/)
 - Read [Shifting left with Gauge](https://gauge.org/2020/06/24/home-loan-application-user-story/)
 - Read [The importance of Shift-Left and Shift-Right testing](https://www.cigniti.com/blog/importance-shift-left-shift-right-testing-approaches/)

### 13) Legacy Code
 - Read chapter 1,2,4,8,13 and 25 of **Working effectively with Legacy Code**
 - Read [Characterization Testing](https://michaelfeathers.silvrback.com/characterization-testing) (OPTIONAL)
 - Read [Working effectively with Legacy Tests](http://natpryce.com/articles/000813.html) (OPTIONAL)
 - Read [Pattern: Strangler Application](https://microservices.io/patterns/refactoring/strangler-application.html) (OPTIONAL)
 - Watch [All the little Things](https://www.youtube.com/watch?v=8bZh5LMaSmE) (OPTIONAL)
 - Watch [Testing and refactoring Legacy code](https://www.youtube.com/watch?v=_NnElPO5BU0) (OPTIONAL)
 - Watch [Surviving a legacy codebase](https://www.youtube.com/watch?v=NGfvguzMjqw) (OPTIONAL)

### 14) Mobile
 - Try [Jetpack Compose](https://developer.android.com/compose)
 - Look at [Material 3 UI](https://m3.material.io/)

### Exercises

- [Coding Dojo Katas](http://codingdojo.org/kata/)
- [Emily Bache Katas](http://coding-is-like-cooking.info/category/code-kata/)